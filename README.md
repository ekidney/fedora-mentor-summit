# Fedora Mentor Summit

Fedora Mentor Summit is an event for Fedora Community Members interested in Mentorship as a practice to share experiences, learn, grow, and network while having fun.

## What is this place?
This repository is our planning and task tracker for the Fedora Mentor Summit.

## About Fedora Mentor Summit (FMS) 2024
Mentor Summit: (Saturday, August 10th) Half-day event focused on mentoring best practices. Mentor Summit programming focuses on workshops and sessions to promote mentorship best practices and to connect mentors and mentees across the Fedora community.

Also this year, FMS will be together with Flock. Flock will be held from Wednesday, August 7th to Saturday, August 10th at the Hyatt Regency Rochester in Rochester, New York, USA.

**When:** August 10, 2024

**Where:** Rochester, New York, USA

**Team:**
* Project management: @jonatoni & @jwflory
* Event production: @jonatoni & @jwflory
* Speaker management: @amsharma3 (lead)
* Marketing: (lead?), @obazeeconsole
* Design: @ekidney (lead), @obazeeconsole

## How to contact the team

Other than this repository, you can also find us on the DEI team Matrix room:

* [Matrix](https://matrix.to/#/#dei:fedoraproject.org)




